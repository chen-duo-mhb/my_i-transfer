#include "pwm.h"

void PWM_Init_TIM1(uint16_t psc,uint16_t arr)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_OCInitTypeDef TIM_OCInitStruct;
	//使能时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE);
	//使能GPIO
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_AF_PP;	
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_8|GPIO_Pin_11;	//PA8&PA11
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	//定时器基本配置
	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStruct);
	TIM_TimeBaseInitStruct.TIM_ClockDivision=TIM_CKD_DIV1;	//不分割
	TIM_TimeBaseInitStruct.TIM_CounterMode=TIM_CounterMode_Up;	//向上计数
	TIM_TimeBaseInitStruct.TIM_Period=arr;		//自动重装载值
	TIM_TimeBaseInitStruct.TIM_Prescaler=psc;			//预分频系数
	TIM_TimeBaseInit(TIM1,&TIM_TimeBaseInitStruct);	
	//输出比较结构体初始化
	TIM_OCInitStruct.TIM_OCMode=TIM_OCMode_PWM1;
	TIM_OCInitStruct.TIM_OCPolarity=TIM_OCPolarity_High;
	TIM_OCInitStruct.TIM_OutputState=TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse=0;
	TIM_OC1Init(TIM1,&TIM_OCInitStruct);	
	TIM_OC4Init(TIM1,&TIM_OCInitStruct);
	
	TIM_CtrlPWMOutputs(TIM1,ENABLE);	//高级定时器专属--MOE主输出使能

	TIM_OC1PreloadConfig(TIM1,TIM_OCPreload_Enable);//OC1预装载寄存器使能
	TIM_OC4PreloadConfig(TIM1,TIM_OCPreload_Enable);//OC4预装载寄存器使能
	TIM_ARRPreloadConfig(TIM1,ENABLE);	//TIM1在ARR上的预装载寄存器使能
	//开启定时器
	TIM_Cmd(TIM1,ENABLE);

}

