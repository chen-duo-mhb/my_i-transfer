#include "YOLOV3_USART.H"

/**
 * @brief K210初始化
 * @param  波特率
 * @retval  无
 */
void yolov3_uart_init(u32 bound)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	// 打开串口 GPIO 的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	// 打开串口外设的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	// 将 USART Tx 的 GPIO 配置为推挽复用模式
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// 将 USART Rx 的 GPIO 配置为浮空输入模式
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// 配置串口的工作参数
	// 配置波特率
	USART_InitStructure.USART_BaudRate = bound;
	// 配置 针数据字长
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	// 配置停止位
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	// 配置校验位
	USART_InitStructure.USART_Parity = USART_Parity_No;
	// 配置硬件流控制
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	// 配置工作模式，收发一起
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	// 完成串口的初始化配置
	USART_Init(USART1, &USART_InitStructure);

	/* 配置 USART 为中断源 */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	/* 抢断优先级为 1 */
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	/* 子优先级为 1 */
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	/* 使能中断 */
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	/* 初始化配置 NVIC */
	NVIC_Init(&NVIC_InitStructure);

	// 使能串口接收中断
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	// 使能串口
	USART_Cmd(USART1, ENABLE);
}

unsigned char numGet = 0;
unsigned char Isnumget_OK = 0;
unsigned char firstNum = 0;

/**
 * @brief 得到第一个数
 * @param 无
 * @retval 无
 */
void getFirst(unsigned char t)
{
	static unsigned char isfirst = 1;

	if (isfirst == 1)
	{
		firstNum = t;
		isfirst = 0;
	}
	else
	{
		return;
	}
	return;
}

/**
 * @brief 接收K210数据包
 * @param  无
 * @retval  无
 */
void USART1_IRQHandler(void)
{
	static unsigned char flag = 0;
	static unsigned char buf = 0;
	static uint8_t temp;

	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		temp = USART_ReceiveData(USART1);

		if ((flag == 0) && (temp == 0X3A)) //判断第一个帧头
		{
			flag = 1;
		}
		else if (flag == 1)
		{
			buf = temp;
			flag = 2;
		}
		else if ((flag == 2) && (temp == 0X5C)) //判断第二个帧头
		{
			if (buf > 0 && buf < 9) //判断是否为正确数据
			{
				getFirst(buf);
				numGet = buf;
				Isnumget_OK = 1;
			}
			flag = 0;
			buf = 0;
		}
		else
		{
			flag = 0;
			buf = 0;
		}
	}
}
