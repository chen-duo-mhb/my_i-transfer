#ifndef __CONTROL_H
#define __CONTROL_H

#include "stm32f10x.h"
#include "sys.h"
#include "encoder.h"
//#include "yolov3_usart.h"
//#include "line_usart.h"
#include "motor.h"
#include "timer.h"
//#include "zigbee.h"


#define Mode_Forward 1  //前进
#define Mode_Back    0  //后退

#define Mode_Left    1  //向左
#define Mode_Right   2  //向右

#define Tager_rho    19 //循迹中值

#define servo_straight    85   //舵机回正
#define servo_Left        105   //舵机向左
#define servo_Right       65   //舵机向右

#define Turnleft_Pluse_Go 1350 //去时向左转向脉冲
#define Turnleft_Pluse_Back 1600 //回时向左转向脉冲
#define Turnrignt_Pluse_Go  1600 //去时向右转向脉冲
#define Turnright_Pluse_Back 1800 //回时向右转向脉冲
#define Turnback             2400    //掉头脉冲

#define Go_near_bybios    7800//到近端十字路口循迹部分脉冲值
#define Go_near_openloop  2400//到近端十字路口开环部分脉冲值
#define Go_near_back      8500//从近端十字路口回到出发点脉冲值

#define Go_middle         20000   //到中端十字路口前识别数字

#define Go_near_room 4500//近端拐弯后前进距离





float Velocity_PID(int Now_Speed,int Taget_Speed);
int Distant_Limit(int Now_Disdant,int*pwm,int Taget_Distant);
void Distant_GO(int Taget_Distant,int pwm,int mode);
void open_loop(int distant,int pwm);
int Line_PWM(int rho,int Taget_rho);
void Car_turn(int pluse,int mode);



#endif //__CONTROL_H



