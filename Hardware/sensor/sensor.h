#ifndef __SENSOR_H
#define __SENSOR_H

#include "stm32f10x.h"

void sensor_init(void);
int read_sensor(void);

#endif //__SENSOR_H


