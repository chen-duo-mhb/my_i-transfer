#include "zigbee.h"
#include "stdio.h"

/*********************************
函数功能:zigbee串口初始化
入口参数:波特率
返回值:无
*********************************/
void zigbee_uart_init(u32 bound)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	// USART2_TX   GPIOA.2
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// USART2_RX	  GPIOA.3初始化
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// USART NVIC 配置
	NVIC_InitStruct.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	// USART 初始化配置
	USART_InitStruct.USART_BaudRate = bound;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStruct);

	//串口开启
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART2, ENABLE);
}

/*********************************
函数功能:zibee发送一个字节
入口参数:串口几
				 发送的数据
返回值:无
*********************************/
void zigbee_sendbyte(USART_TypeDef *pUSARTx, unsigned char data)
{
	USART_SendData(pUSARTx, data);
	while (USART_GetFlagStatus(pUSARTx, USART_FLAG_TXE) == RESET)
		;
}

/**
 * @brief 发送vofa格式数据(JustFloat)
 * @param 串口号;浮点型数据;
 * @retval
 */
void zigbee_vofa(USART_TypeDef *pUSARTx, float *data, int lenth)
{
	zigbee_send_array(pUSARTx, (uint8_t *)data, lenth);
	zigbee_sendbyte(pUSARTx, 0X00);
	zigbee_sendbyte(pUSARTx, 0X00);
	zigbee_sendbyte(pUSARTx, 0X80);
	zigbee_sendbyte(pUSARTx, 0X7F);
}

/*********************************
函数功能:zigbee发送一个数组
入口参数:串口几
				 发送的数组
				 数组长度
返回值:无
*********************************/
void zigbee_send_array(USART_TypeDef *pUSARTx, unsigned char *array, int lenth)
{
	unsigned char i;

	for (i = 0; i < lenth; i++)
	{
		zigbee_sendbyte(pUSARTx, array[i]);
	}

	while (USART_GetFlagStatus(pUSARTx, USART_FLAG_TC) == RESET)
		;
}

/**
  * @brief  次方函数
  * @param  x:底数
						y:指数
  * @retval x的y次方
  */
uint32_t Pow(uint32_t x, uint32_t y)
{
	uint32_t result = 1;
	while (y--)
	{
		result *= x;
	}
	return result;
}

/**
  * @brief  以字符形式发送数据
  * @param  Number:发送的数
						Length:数字长度
  * @retval 无
  */
void Zigbee_SendNumber(uint16_t Number, uint8_t Length)
{
	uint8_t i;
	for (i = 0; i < Length; i++)
	{
		zigbee_sendbyte(USART2, Number / Pow(10, Length - i - 1) % 10 + '0');
	}
}

char recv_array[4] = {0};

//串口2中断服务函数
void USART2_IRQHandler(void)
{
	unsigned char recvdata;
	static unsigned char recvBuf[7] = {0};
	static unsigned char recvIndex = 0;
	static unsigned char recvFlag = 0;

	GPIO_InitTypeDef GPIO_InitStruct;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET) //接收中断
	{

		recvdata = USART_ReceiveData(USART2);

		//
		if (recvFlag == 0 && recvdata == 0X7F) //检测帧头
		{

			recvFlag = 1;
			recvIndex = 0; //索引为0，开始接受
		}

		if (recvFlag == 1) //如果允许接受
		{
			if (recvIndex < 7)
			{

				recvBuf[recvIndex] = recvdata; //暂存
				recvIndex++;				   //索引偏移
			}
			else //索引运动限制
			{
				recvIndex = 0;														//清空索引
				if (recvBuf[0] == 0X7F && recvBuf[1] == 0X80 && recvBuf[6] == 0X81) //如果符合协议
				{
					recv_array[0] = recvBuf[2];
					recv_array[1] = recvBuf[3];
					recv_array[2] = recvBuf[4];
					recv_array[3] = recvBuf[5];
				}
				recvFlag = 0;
			}
		}
		else //未知异常
		{

			recvIndex = 0; //接受索引
			recvFlag = 0;  //接受标志位
		}
	}

	USART_ClearITPendingBit(USART2, USART_IT_RXNE);
}
