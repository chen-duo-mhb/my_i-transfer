### Openmv循迹部分

<img src="file:///C:/Users/Wangjiawei/AppData/Roaming/marktext/images/2022-10-29-08-24-27-image.png" title="" alt="" width="367">

## 方法一、直接通过对颜色（色块）进行循迹

```python
import sensor, image, time
from pyb import UART
import json
from pyb import LED

#led = LED(1)#红
#led.on()
#led = LED(2)#绿
#led.on()
#led = LED(3)#蓝
#led.on()

red_threshold  = (0,46,-58,54,-88,36)#循迹颜色阈值
sensor.reset() 
sensor.set_pixformat(sensor.RGB565) 
sensor.set_framesize(sensor.QQQVGA) 
sensor.skip_frames(10) 
sensor.set_auto_whitebal(False) 
clock = time.clock() 

uart = UART(3, 115200)
uart.init(115200, bits=8, parity=None, stop=1)  #8位数据位，无校验位，1位停止位

def find_max(blobs):
    max_size=0
    for blob in blobs:
        if blob[2]*blob[3] > max_size:
            max_blob=blob
            max_size = blob[2]*blob[3]
    return max_blob
while(True):
    clock.tick() # Track elapsed milliseconds between snapshots().
    img = sensor.snapshot() # Take a picture and return the image.

    blobs = img.find_blobs([red_threshold])
    if blobs:
        is_cross = 0 #是否为交叉路口
        max_blob = find_max(blobs)
        img.draw_cross(max_blob.cx(),max_blob.cy())
        img.draw_circle(max_blob.cx(),max_blob.cy(),max_blob.cx()-max_blob.x(), color = (255, 255, 255))
        X =int(max_blob.cx()-img.width()/2+10)
        Y =int(max_blob.cy()-img.height()/2)
        max_size = int(max_blob.w())#宽度判断
        if max_size > 71:#宽度阈值（交叉路口阈值）
            is_cross = 1
        data = bytearray([0xb3,0xb3,X,Y,is_cross,0x5b])
        uart.write(data)    #打印XY轴的偏移坐标


        print("X轴偏移坐标 : ",X)
        print("Y轴偏移坐标 : ",Y)
        print("是否为十字路口  ：",is_cross)


       # print("帧率 : ",clock.fps())
```

## 方法二、线性回归

![](C:\Users\Wangjiawei\AppData\Roaming\marktext\images\2022-11-03-20-18-53-image.png) 

```python
THRESHOLD = (0,46,-58,54,-88,36) #循迹阈值
import sensor, image, time
from pyb import LED
from pyb import UART



uart = UART(3, 115200)
uart.init(115200, bits=8, parity=None, stop=1)  #8位数据位，无校验位，1位停止位

# LED(1).on()
# LED(2).on()
# LED(3).on()

sensor.reset()
sensor.set_vflip(True)
sensor.set_hmirror(True)
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QQQVGA) # 80x60 (4,800 pixels) - O(N^2) max = 2,3040,000.
#sensor.set_windowing([0,20,80,40])
sensor.skip_frames(time = 2000)     # WARNING: If you use QQVGA it may take seconds
clock = time.clock()                # to process a frame sometimes.

while(True):
    clock.tick()
    img = sensor.snapshot()
    img.mean(2)#降噪
    img.binary([THRESHOLD])#二值化
    img.crop([0,15,80,60])#截取图像
    img.erode(1)#加粗线条
    line = img.get_regression([(100,100)], robust = True)
    if (line):
        rho =(int)(abs(line.rho())-img.width()/2)
        if line.theta()>90:
            theta = line.theta()-180
        else:
            theta = line.theta()
        img.draw_line(line.line(), color = 127)

        data = bytearray([0xb3,0xb3,rho,theta,0x5b])
        uart.write(data)    #打印rho和偏移坐标
        print("rho:",rho)      #回归线的偏移距离   一般用偏移距离较好
        print("theta:",theta)  #回归线的偏移角度
```

